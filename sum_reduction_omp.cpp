#include<cstdio>
#include<cmath>
#include<chrono>
#include<omp.h>
#include<algorithm>

void init_array(int* x, int n){
	for(int i = 0; i < n; i++){
		x[i] = 1;
	}
}

int main() {

	int n = 1 << 16;
	/* allocate array in heap */
	int* array = (int*)malloc(n * sizeof(int));
	
	/* init sum and array*/
	int sum = 0;	
    init_array(array, n);
    
    /* start time */
	uint64_t t0 = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count();     
    
    /* compute sum reduction */
    #pragma omp parallel for shared(n, array) reduction(+:sum)
    for(int i = 0; i < n; i++){
    	sum += array[i];
    }
    
    /* finish time */
	uint64_t t1 = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count();     
    
    printf("OMP CPU in %luus\nSum result : %d \n", t1-t0, sum);

    free(array);
    return 0;
}
