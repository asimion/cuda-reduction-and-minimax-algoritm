# CUDA Reduction And Minimax Algorithm



## Prerequisites

CUDA Toolkit 11.6

## Description

Impletemtation of sum reduction algorithm in CUDA and comparison with the OpenMP version on the CPU.
 
Implementation of minimax algorithm for TIC TAC TOE in CUDA and comparison with the serial version on the CPU.

Impletemtation of max reduction algorithm in CUDA and comparison with the OpenMP version on the CPU.



