#include<cstdio>
#include<cmath>
#include<chrono>
#include<algorithm>

const unsigned SHMEM_SIZE = 256 * 4;
const unsigned SIZE = 256;

void init_array(int* x, int n){
	for(int i = 0; i < n; i++){
		x[i] = 1;
	}
}

__global__ void sum_reduction(int* a, int* a_r) {
	__shared__ int partial_sum[SHMEM_SIZE];
	
	//Calc thread ID
	int tid = blockIdx.x * blockDim.x + threadIdx.x;
	
	//Load elems into shared mem and perform first add of reduction
	int i = blockIdx.x * (blockDim.x * 2) + threadIdx.x;
	partial_sum[threadIdx.x] = a[tid] + a[i + blockDim.x];
	__syncthreads();
	
	//reduction
    for(int stride = blockDim.x/2; stride > 0; stride >>= 1){
    	if(threadIdx.x < stride){
    		partial_sum[threadIdx.x] += partial_sum[threadIdx.x + stride];
    	}
    __syncthreads();
    }
    
    if(threadIdx.x == 0){
    	a_r[blockIdx.x] = partial_sum[0];
    }
}

int main(){
	unsigned n = 1 << 16;
	int TB_SIZE = SIZE;
	int GRID_SIZE = (int)ceil(n/TB_SIZE/2);
	
	/* host */
	int* h_array;
	int* h_array_r;
	
	/* device */
	int* d_array;
	int* d_array_r;
	
	/* size */
	size_t bytes_array = n * sizeof(int);
	size_t bytes_array_r = n * sizeof(int);	
	
	/* allocate heap memory host side*/
	h_array = (int*)malloc(bytes_array);
	h_array_r = (int*)malloc(bytes_array_r);
	
	/* allocate mem on device */
	cudaMalloc(&d_array, bytes_array);
	cudaMalloc(&d_array_r, bytes_array_r);
	
	/* init host_array */
	init_array(h_array, n);
	
    /* copy data to device */	
	cudaMemcpy(d_array, h_array, bytes_array, cudaMemcpyHostToDevice);
	
    /* start time */
	uint64_t t0 = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count(); 
	
	sum_reduction<<<GRID_SIZE, TB_SIZE>>>(d_array, d_array_r);
	sum_reduction<<<1, TB_SIZE>>>(d_array_r, d_array_r);
	
    /* finish time */
	uint64_t t1 = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count(); 
	
	cudaError_t cudaerr = cudaDeviceSynchronize();
    if (cudaerr != cudaSuccess)
        printf("kernel launch failed with error \"%s\".\n", cudaGetErrorString(cudaerr));
        
    
    cudaMemcpy(h_array_r, d_array_r, bytes_array_r, cudaMemcpyDeviceToHost);
    printf("GPU COMPLETED IN %luus\n", t1-t0);
    printf("Accumulated result : %d \n", h_array_r[0]);
    
    free(h_array);
    free(h_array_r);

    cudaFree(d_array);
    cudaFree(d_array_r);
	return 0;
}
