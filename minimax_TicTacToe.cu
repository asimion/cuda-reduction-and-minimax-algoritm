#include<cstdio>
#include<algorithm>
#include<cstring>
#include<chrono>

const unsigned DIM_BOARD = 3;
const int MINIMUM_SCORE = -1000;
const int MAXIMUM_SCORE =  1000;

enum state_game {
	DRAW = 0,
	X_WON = 1,
	O_WON = -1,
	DontKnow = 404
};

typedef struct Move{
	int row;
	int col;
} MOVE;

__device__ bool isMovesLeft(char *BOARD)
{
	for (int i = 0; i < DIM_BOARD; i++)
		for (int j = 0; j < DIM_BOARD; j++)
			if (BOARD[i * DIM_BOARD + j] == '#' )
				return true;
	return false;
}

__device__ int check_winner( char *BOARD) {
	
	for (int row = 0; row < DIM_BOARD; row++)
	{
		if (BOARD[row * DIM_BOARD + 0] == BOARD[row * DIM_BOARD + 1] &&
			BOARD[row * DIM_BOARD + 1] == BOARD[row * DIM_BOARD + 2])
		{
			if (BOARD[row * DIM_BOARD + 0] == 'O')
				return O_WON; //human score
			else if (BOARD[row * DIM_BOARD + 0] == 'X')
				return X_WON; //ai score
		}
	}

	// Checking for Columns for X or O victory.
	for (int col = 0; col < DIM_BOARD; col++)
	{
		if (BOARD[0 * DIM_BOARD + col] == BOARD[1 * DIM_BOARD + col] &&
			BOARD[1 * DIM_BOARD + col] == BOARD[2 * DIM_BOARD + col])
		{
			if (BOARD[0 * DIM_BOARD + col] == 'O')
				return O_WON;
			else if (BOARD[0 * DIM_BOARD + col] == 'X')
				return X_WON;
		}
	}

	// Checking for Diagonals for X or O victory.
	if (BOARD[0 * DIM_BOARD + 0] == BOARD[1 * DIM_BOARD + 1] && BOARD[1 * DIM_BOARD + 1] == BOARD[2 * DIM_BOARD + 2])
	{
		if (BOARD[0 * DIM_BOARD + 0] == 'O')
			return O_WON;
		else if (BOARD[0 * DIM_BOARD + 0] == 'X')
			return X_WON;
	}

	if (BOARD[0 * DIM_BOARD + 2] == BOARD[1 * DIM_BOARD + 1] && BOARD[1 * DIM_BOARD + 1] == BOARD[2 * DIM_BOARD + 0])
	{
		if (BOARD[0 * DIM_BOARD + 2] == 'O')
			return O_WON;
		else if (BOARD[0 * DIM_BOARD + 2] == 'X')
		    return X_WON;
	}


    // If there are no more moves and no winner then
	// it is a tie
	if (isMovesLeft(BOARD) == false) return DRAW;
	
	// Else if none of them have won then return 0
	return DontKnow;
}

__device__ int minimax( char *BOARD, int depth, bool isMaximizing){
	int score = check_winner(BOARD);
	//printf("score depth %d %d\n", score, depth);
	if(score != DontKnow) return score;
	
	// If this maximizer's move
	if (isMaximizing) {
		int best = MINIMUM_SCORE;
		// Traverse all cells
		for (int i = 0; i < DIM_BOARD; i++) {
			for (int j = 0; j < DIM_BOARD; j++) {
				// Check if cell is empty
				if (BOARD[i * DIM_BOARD + j] == '#') {   
					// Make the move
					BOARD[i * DIM_BOARD + j] = 'X';
					// Call minimax recursively and choose
					// the maximum value
					best = std::max( best, minimax(BOARD, depth + 1, !isMaximizing) );
					// Undo the move
					BOARD[i * DIM_BOARD + j] = '#';
				}
			}
		}
		return best;
	} else {// If this minimizer's move
			int best = MAXIMUM_SCORE;
			// Traverse all cells
			for (int i = 0; i < DIM_BOARD; i++) {
				for (int j = 0; j < DIM_BOARD; j++) {
					// Check if cell is empty
					if (BOARD[i * DIM_BOARD + j] == '#') {
						// Make the move
						BOARD[i * DIM_BOARD + j] = 'O';
						// Call minimax recursively and choose
						// the minimum value
						best = std::min(best, minimax(BOARD, depth + 1, !isMaximizing) );
						// Undo the move
						BOARD[i * DIM_BOARD + j] = '#';
					}
				}
			}
			return best;
		}
}

__global__ void best_ai_move( char *board, MOVE *best_move ){
	int row = blockIdx.y * blockDim.y + threadIdx.y;
	int col = blockIdx.x * blockDim.x + threadIdx.x;
	char board_loc[DIM_BOARD*DIM_BOARD];
    __shared__ MOVE moves[DIM_BOARD*DIM_BOARD];
    __shared__ int idx;
    
    idx = 0;//counter
    __syncthreads();
    
    /* init local board */
    for(int i = 0; i < DIM_BOARD; i++){
    	for(int  j = 0; j < DIM_BOARD; j++){
    		board_loc[i*DIM_BOARD+j] = board[i*DIM_BOARD+j];
    	}
    }
    // each thread moves if their cell is free
    if(board[row * DIM_BOARD + col] == '#') {
        board_loc[row * DIM_BOARD + col] = 'X';
        int score = minimax(board_loc, 0, false);
	    board_loc[row * DIM_BOARD + col] = '#';
        //printf("row col bs: %d %d %d\n", row, col, score);
       	if(score >= 1) {
       	    moves[idx].row = row;
       	    moves[idx].col = col;
       	    idx++;
	    }
	 } 
	 __syncthreads();

	   *best_move = moves[0];   
}		

int main(){
    int dim_board = DIM_BOARD;
    
    //host pointers;
    char* h_board;
    MOVE* h_best_move;

    //device pointers;
    char* d_board;
    MOVE* d_best_move;
    
    //sizes  
    size_t bytes_board = dim_board * dim_board * sizeof(char);
    size_t bytes_move = sizeof(MOVE); 
    
    //allocate memory on host side
    h_board = (char*)malloc(bytes_board);
    h_best_move = (MOVE*)malloc(bytes_move);

    //init board
    for(int i = 0; i < dim_board; i++){
    	for(int j = 0; j < dim_board; j++){
    		h_board[i * dim_board + j] = '#';
    	}
    }
    h_board[0 * dim_board + 0] = 'X';
    h_board[0 * dim_board + 2] = 'O';
    h_board[1 * dim_board + 0] = 'X';
    h_board[2 * dim_board + 0] = 'O';
    
    //allocate memory on device side;
    cudaMalloc(&d_board, bytes_board);
    cudaMalloc(&d_best_move, bytes_move);
    
    //Copy data on the device
    cudaMemcpy(d_board, h_board, bytes_board, cudaMemcpyHostToDevice);
    
    int threads_per_block = DIM_BOARD;
    dim3 block_size(threads_per_block, threads_per_block);
    dim3 grid_size(dim_board / block_size.x,  dim_board / block_size.y);    
    
    //cudaDeviceSetLimit(cudaLimitStackSize, 4096);
  
    /* start time */
	uint64_t t0 = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count(); 

    /*start kernel */
    best_ai_move<<<grid_size, block_size>>> (d_board, d_best_move);
    
    /* finish time */
    uint64_t t1 = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count();

    cudaError_t cudaerr = cudaDeviceSynchronize();
    if (cudaerr != cudaSuccess)
        printf("kernel launch failed with error \"%s\".\n", cudaGetErrorString(cudaerr));

    //Copy data from device to host    
    cudaMemcpy(h_best_move, d_best_move, bytes_move, cudaMemcpyDeviceToHost);
    printf("\nCOMPLETED GPU in %luus\n\n", t1 - t0);
    
    printf("CURRENT BOARD : \n");
    for(int i = 0; i < dim_board; i++){
    	for(int j = 0; j < dim_board; j++){
    		printf("%c ", h_board[i * dim_board + j]);
    	}
    	printf("\n");
    }
    
    printf("Best move ai :%d %d\n", h_best_move->row, h_best_move->col);
    
    free(h_board);
    free(h_best_move);

    cudaFree(d_board);
    cudaFree(d_best_move);

	return 0;
}
