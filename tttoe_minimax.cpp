// C++ minimax algorithm TIC_TAC_TOE
#include<cstdio>
#include<iostream>
#include<algorithm>
#include<cstring>
#include<chrono>

using namespace std;

#define DIM_N 3
#define DIM_M 3


enum state_game {
	DRAW=0,
	X_WON=1,
	O_WON=-1,
	DK=404
};

typedef struct Move{
	int row;
	int col;
} MOVE;

typedef struct ai{
	char symbol = 'X';
    int score = 1;
}AI;

typedef struct human{
	char symbol = 'O';
	int score = -1;
}HUMAN;

bool isMovesLeft(char BOARD[DIM_N][DIM_M])
{
	for (int i = 0; i < DIM_N; i++)
		for (int j = 0; j < DIM_M; j++)
			if (BOARD[i][j] == '#')
				return true;
	return false;
}


AI ai;
HUMAN human;

int evaluate(char BOARD[DIM_N][DIM_M])
{
	// Checking for Rows for X or O victory.
	for (int row = 0; row < DIM_N; row++)
	{
		if (BOARD[row][0] == BOARD[row][1] &&
			BOARD[row][1] == BOARD[row][2])
		{
			if (BOARD[row][0] == human.symbol)
				return human.score;
			else if (BOARD[row][0] == ai.symbol)
				return ai.score;
		}
	}

	// Checking for Columns for X or O victory.
	for (int col = 0; col < DIM_M; col++)
	{
		if (BOARD[0][col] == BOARD[1][col] &&
			BOARD[1][col] == BOARD[2][col])
		{
			if (BOARD[0][col] == human.symbol)
				return human.score;

			else if (BOARD[0][col] == ai.symbol)
				return ai.score;
		}
	}

	// Checking for Diagonals for X or O victory.
	if (BOARD[0][0] == BOARD[1][1] && BOARD[1][1] == BOARD[2][2])
	{
		if (BOARD[0][0] == human.symbol)
			return human.score;
		else if (BOARD[0][0] == ai.symbol)
			return ai.score;
	}

	if (BOARD[0][2] == BOARD[1][1] && BOARD[1][1] == BOARD[2][0])
	{
		if (BOARD[0][2] == human.symbol)
			return human.score;
		else if (BOARD[0][2] == ai.symbol)
			return ai.score;
	}


    // If there are no more moves and no winner then
	// it is a tie
	if (isMovesLeft(BOARD)==false) return DRAW;
	
	// Else if none of them have won then return 0
	return DK;
}


int minimax(char BOARD[DIM_N][DIM_M], int depth, bool isMax)
{
	int score = evaluate(BOARD);
    if(score != DK) return score;
    

	// If this maximizer's move
	if (isMax)
	{
		int best = -1000;

		// Traverse all cells
		for (int i = 0; i < DIM_N; i++)
		{
			for (int j = 0; j < DIM_M; j++)
			{
				// Check if cell is empty
				if (BOARD[i][j]=='#')
				{
					// Make the move
					BOARD[i][j] = ai.symbol;

					// Call minimax recursively and choose
					// the maximum value
					best = max( best, minimax(BOARD, depth + 1, !isMax) );

					// Undo the move
					BOARD[i][j] = '#';
				}
			}
		}
		return best;
	}

	// If this minimizer's move
	else
	{
		int best = 1000;

		// Traverse all cells
		for (int i = 0; i < DIM_N; i++)
		{
			for (int j = 0; j < DIM_M; j++)
			{
				// Check if cell is empty
				if (BOARD[i][j] == '#')
				{
					// Make the move
					BOARD[i][j] = human.symbol;

					// Call minimax recursively and choose
					// the minimum value
					best = min(best, minimax(BOARD, depth + 1, !isMax) );

					// Undo the move
					BOARD[i][j] = '#';
				}
			}
		}
		return best;
	}
}

MOVE findBestMove(char BOARD[DIM_N][DIM_M])
{
	int bestVal = -1000;
	MOVE bestMove;
	bestMove.row = -1;
	bestMove.col = -1;

	// Traverse all cells, evaluate minimax function for
	// all empty cells. And return the cell with optimal
	// value.
	for (int i = 0; i < DIM_N; i++)
	{
		for (int j = 0; j < DIM_M; j++)
		{
			// Check if cell is empty
			if (BOARD[i][j] == '#')
			{
				// Make the move
				BOARD[i][j] = ai.symbol;

				// compute evaluation function for this
				// move.
				int moveVal = minimax(BOARD, 0, false);

				// Undo the move
				BOARD[i][j] = '#';

				// If the value of the current move is
				// more than the BOARDest value, then update
				// BOARDest/
				if (moveVal > bestVal)
				{
					bestMove.row = i;
					bestMove.col = j;
					bestVal = moveVal;
				}
			}
		}
	}


	return bestMove;
}

void print_BOARD(char BOARD[DIM_N][DIM_M]){
    printf("CURRENT BOARD STATE \n\n");
   for(int i = 0 ;i < DIM_N; i++){
       for(int j = 0; j < DIM_M; j++){
            if(j == DIM_N-1 ) printf(" %c ", BOARD[i][j]);
            else printf(" %c |", BOARD[i][j]);
       }
       printf("\n\r - - - - - -\n\r");
   } 
}   

int stop_game(char BOARD[DIM_N][DIM_M]){
	switch(evaluate(BOARD)){
		case X_WON : { printf("AI WON!");    return 0; }
	    case O_WON : { printf("HUMAN WON!"); return 0; }
	    case DRAW    : { printf("IT's A TIE!"); return 0; } 	
	}
	return DK;	
}

// Driver code
int main()
{   

    MOVE bestMove ;
	char BOARD[DIM_N][DIM_M] =
	{
		{ 'X', '#', 'O' },
		{ 'X', '#', '#' },
		{ 'O', '#', '#' }
	};
	
    print_BOARD(BOARD);
    
    /* for testing */
    uint64_t t0 = std::chrono::duration_cast<std::chrono::microseconds>(
    std::chrono::system_clock::now().time_since_epoch()
).count();
    bestMove = findBestMove(BOARD);
    uint64_t t1 = std::chrono::duration_cast<std::chrono::microseconds>(
    std::chrono::system_clock::now().time_since_epoch()
).count();
    printf("\nCOMPLETED CPU in %luus\n\n", t1 - t0);
    printf("Best move ai :%d %d\n", bestMove.row, bestMove.col);
    
    /* game loop() */
    /*
    while(true){
		bestMove = findBestMove(BOARD);
		BOARD[bestMove.row][bestMove.col] = ai.symbol;
		MOVE pmove;
		print_BOARD(BOARD);
		if(stop_game(BOARD) != DK) return 0;
		printf("Player row, col : ");scanf("%d %d", &pmove.row, &pmove.col); 
		BOARD[pmove.row][pmove.col] = human.symbol;
		print_BOARD(BOARD);
    }
    */
	return 0;
}

