#include<cstdio>
#include<cmath>
#include<chrono>
#include<omp.h>
#include<algorithm>

void init_array(int* x, int n){
	for(int i = 0; i < n; i++){
		x[i] = i % 100;
		x[(2*i+5)%n] = i % 7898;
	}
	x[n/2 + 15] = 8080; // hardcode a max value
}
int main() {

	int n = 1 << 20;
	/* allocate array in heap */
	int* array = (int*)malloc(n * sizeof(int));
	
	/* init max and array*/
	int mmax = 0;	
    init_array(array, n);
    
    /* start time */
	uint64_t t0 = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count();     
    
    /* compute sum reduction */
    #pragma omp parallel for shared(n, array) reduction(std::max: mmax)
    for(int i = 0; i < n; i++){
    	mmax = std::max(mmax, array[i]);
    }
    
    /* finish time */
	uint64_t t1 = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count();     
    
    printf("OMP CPU in %luus\nMaxim : %d \n", t1-t0, mmax);
 
    free(array);
    return 0;
}
