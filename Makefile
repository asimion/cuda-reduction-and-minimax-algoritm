.PHONY: help
help:
	@echo "make [gpu_minimax/gpu_sum/gpu_max/cpu_minimax/cpu_omp_sum/cpU_omp_max] to build specific binary"
	@echo "make mrproper to clean"

gpu_minimax: minimax_TicTacToe.cu
	nvcc --expt-relaxed-constexpr $^ -o $@

gpu_max: max_reduction.cu
	nvcc --expt-relaxed-constexpr $^ -o $@

gpu_sum: sum_reduction.cu
	nvcc $^ -o $@

cpu_minimax: tttoe_minimax.cpp
	g++ $^ -o $@
	
cpu_omp_sum: sum_reduction_omp.cpp
	g++ -fopenmp $^ -o $@

cpu_omp_max: max_reduction_omp.cpp
	g++ -fopenmp $^ -o $@

.PHONY: mrproper
mrproper:
	rm -f gpu_minimax gpu_sum gpu_max cpu_minimax cpu_omp_sum cpu_omp_max
