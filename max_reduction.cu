#include<cstdio>
#include<cmath>
#include<chrono>
#include<algorithm>

void init_array(int* x, int n){
	for(int i = 0; i < n; i++){
		x[i] = i % 100;
		x[(2*i+5)%n] = i % 7898;
	}
	x[n/2 + 15] = 8080; // hardcode a max value
}

__global__ void max_reduction(int* mutex, int* array, int* mmax, unsigned n) {
	unsigned tid = blockIdx.x * blockDim.x + threadIdx.x;
	unsigned stride = gridDim.x * blockDim.x;
	unsigned offset = 0;
	int tmp = 0;
	__shared__ int cache[256];
    
    /*init for every chunk of array with a pseudo max*/
    while(tid + offset < n) {
    	tmp = std::max(tmp, array[tid + offset]);
    	offset += stride;
    }
    
    cache[threadIdx.x] = tmp;
    __syncthreads();
    
    /* Reduction */
    unsigned it = blockDim.x / 2;
    while(it > 0){
    	if(threadIdx.x < it){
    		cache[threadIdx.x] = std::max(cache[threadIdx.x], cache[threadIdx.x + it]);
    	}
    	__syncthreads();
    	it >>= 1;
    }
    
    /* update the maxim into mmax */
    if(threadIdx.x == 0){
    	while(atomicCAS(mutex, 0, 1) != 0);
    	*mmax = std::max(*mmax, cache[0]);
    	atomicExch(mutex, 0);
    }
}

int main(){
	
	unsigned n = 1 << 20;
	
	/* host */
	int* h_vect;
    int* h_max;
    
    /* device */	
	int* d_vect;
	int* d_max;
	int* d_mutex;
	
	/* size */
	size_t bytes_vect = n * sizeof(int);
	size_t bytes_max = sizeof(int);
	size_t bytes_mutex = sizeof(int);
	
	/* allocate memory on host side */
	h_vect = (int*)malloc(bytes_vect);
	h_max = (int*)malloc(bytes_max);
	
	/* allocate memory on device side */
	cudaMalloc(&d_vect, bytes_vect);
	cudaMalloc(&d_max, bytes_max);
	cudaMalloc(&d_mutex, bytes_mutex);
	
	/* init on device side */
	cudaMemset(d_mutex, 0, bytes_mutex);
	cudaMemset(d_max, 0, bytes_max);
	
	/* init on host side */
	init_array(h_vect, n);
	
	/* copy data to device */
	cudaMemcpy(d_vect, h_vect, bytes_vect, cudaMemcpyHostToDevice);
  
    /* setup gridSize, blockSize */
    int GRID_SIZE = 256;
    int BLOCK_SIZE = 256;
    
    /* start time */
	uint64_t t0 = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count(); 
   
    /* kernel */
    max_reduction<<< GRID_SIZE, BLOCK_SIZE >>> (d_mutex, d_vect, d_max, n);
    
    /* finish time */
	uint64_t t1 = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count(); 
    
    /* get an error msg if the kernel crash */
    cudaError_t cudaerr = cudaDeviceSynchronize();
    if (cudaerr != cudaSuccess)
        printf("kernel launch failed with error \"%s\".\n", cudaGetErrorString(cudaerr));
    
    cudaMemcpy(h_max, d_max, bytes_max, cudaMemcpyDeviceToHost);
    
    printf("GPU COMPLETED in %luus\n", t1-t0);
    printf("Maxim : %d\n", *h_max);
    
    /*free memory host , device */
	free(h_vect);
	free(h_max);
	
	cudaFree(d_vect);
	cudaFree(d_max);
	cudaFree(d_mutex);
	
	return 0;
}
